import { Input } from 'phaser';
import { AXIS } from './globals';

export const INPUT_CONTROLS_CONFIG: {[name: string]: {key: number, axis: string, direction: 1 | -1}} = Object.freeze({
  up: {key: Input.Keyboard.KeyCodes.UP, axis: AXIS.y, direction: -1},
  down: {key: Input.Keyboard.KeyCodes.DOWN, axis: AXIS.y, direction: 1},
  right: {key: Input.Keyboard.KeyCodes.RIGHT, axis: AXIS.x, direction: 1},
  left: {key: Input.Keyboard.KeyCodes.LEFT, axis: AXIS.x, direction: -1}
})

export const INPUT_ACTION_CONFIG: {[name: string]: {key: number}} = Object.freeze({
  shoot: {key: Input.Keyboard.KeyCodes.SPACE}
})

export const ACTION_CONFIG = Object.freeze({
  shootCooldownTime: 100
})

export const CONTROLS_CONFIG = Object.freeze({
  velocity: {x: 30, y: 35},
  airControlFactor: 0.7
})

export const PLAYER_CONFIG = Object.freeze({
  health: 100,
  damage: 10,
  textureName: 'hare'
})

export type EnemyData = {
  name: string,
  textureName: string,
  health: number,
  damage: number,
  velocity: { x: number, y: number },
  canShoot: boolean,
  shootCooldownTime: number,
  size: number
}

export const ENEMIES: {[name: string]: EnemyData} = Object.freeze({
  minion: {
    name: 'minion',
    textureName: 'minion',
    health: 20,
    damage: 10,
    velocity: {x: 6, y: 0},
    canShoot: false,
    shootCooldownTime: 0,
    size: 1.0,
  },
  turret: {
    name: 'turret',
    textureName: 'turret',
    health: 10,
    damage: 30,
    velocity: {x: 0, y: 0},
    canShoot: true,
    shootCooldownTime: 1000,
    size: 1.0,
  },
  golem: {
    name: 'golem',
    textureName: 'golem',
    health: 50,
    damage: 20,
    velocity: {x: 4, y: 0},
    canShoot: false,
    shootCooldownTime: 0,
    size: 1.5,
  },
})

export const IMAGES = Object.freeze({
  hare: "hare.png",
  fireball: "fireball.png",
  minion: "minion.png",
  golem: "golem.png",
  turret: "turret.png",
  carrot: "carrot.png",
  spike: "spike.png"
})