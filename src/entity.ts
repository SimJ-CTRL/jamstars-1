import { Math, Physics } from 'phaser'
import Map from './map'
import CustomScene from './main'

export type EntityProperties = {velocity: { x: number, y: number }, health: number, spawnPosition: Phaser.Math.Vector2, scene: CustomScene, map: Map, textureName: string}

export default class Entity extends Physics.Arcade.Sprite  {
  velocity: Math.Vector2
  direction: number

  health: number

  scene: CustomScene
  map: Map

  constructor({ velocity, health, spawnPosition, scene, map, textureName }: EntityProperties) {
    super(scene, spawnPosition.x, spawnPosition.y, textureName)
    
    this.velocity = new Math.Vector2(velocity.x, velocity.y)
    this.direction = 1

    this.health = health

    this.scene = scene
    this.map = map

    this.scene.physics.add.existing(this)
    this.scene.physics.add.collider(this, this.map.tilemapLayer.foreground)
  }

  takeDamage(damage: number) {
    this.health -= damage
  }
  addHealth(health: number) {
    this.health += health
  }
}