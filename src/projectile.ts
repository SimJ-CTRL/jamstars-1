import Entity from "./entity"
import CustomScene from "./main"
import Map from "./map"

export type ProjectileProperties = { scene: CustomScene, damage: number, textureName: string, direction: number, velocity: number, map: Map, spawnPosition: Phaser.Math.Vector2 }

export default class Projectile extends Phaser.GameObjects.Sprite {
  damage: number
  
  constructor({ scene, damage, textureName, velocity, direction, map, spawnPosition }: ProjectileProperties) {
    super(scene, spawnPosition.x, spawnPosition.y, textureName)
    
    this.damage = damage

    this.setFlipX(direction < 0)

    this.scene.physics.add.existing(this)
    // @ts-ignore
    this.body.setVelocityX(velocity * direction)
    // @ts-ignore
    this.body.setAllowGravity(false)

    this.scene.physics.add.collider(
      this,
      map.tilemapLayer.foreground,
      (projectile, object) => {
        projectile.destroy()
      }
    )

    this.scene.physics.add.collider(
      this,
      scene.enemies,
      (source, target) => {
        const projectile = source as Projectile
        const enemy = target as Entity

        enemy.takeDamage(projectile.damage)

        projectile.destroy()
      }
    )

    this.scene.physics.add.collider(
      this,
      scene.player,
      (source, target) => {
        const projectile = source as Projectile
        const player = target as Entity

        player.takeDamage(projectile.damage)

        projectile.destroy()
      }
    )
  }
}