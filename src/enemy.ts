import { ACTION_CONFIG, EnemyData } from './config'
import Entity, { EntityProperties } from './entity'
import Player from './player'
import Projectile from './projectile'

export type EnemyProperties = EntityProperties & EnemyData & { player: Player }

export default class Enemy extends Entity {
  damage: number

  shootCooldown: boolean
  shootCooldownTime: number
  canShoot: boolean

  player: Player

  constructor(properties: EnemyProperties) {
    super(properties)

    this.damage = properties.damage
    this.canShoot = properties.canShoot
    this.shootCooldown = false
    this.shootCooldownTime = properties.shootCooldownTime

    this.player = properties.player
    this.scale = properties.size
  }

  update(deltaTime: number) {
    if(this.health <= 0) {
      this.destroy()
      return
    }

    if(!this.body.blocked.down) {
      this.body.velocity.x = 0
      return
    }

    if(this.body.blocked.left || this.body.blocked.right) {
      this.direction = this.body.blocked.left ? 1 : -1
      this.setFlipX(this.direction < 0)
    }
      
    this.body.velocity.x = this.direction * this.velocity.x * deltaTime

    if(!this.canShoot) return

    const isPlayerInReach = Math.round(Math.abs(this.player.x - this.x)) < 64 * 4 && Math.abs(this.player.y - this.y) < 64
    const isPlayerInFront = this.direction === Math.sign(this.player.x - this.x)

    if(isPlayerInFront && isPlayerInReach && !this.shootCooldown) {
      this.shootCooldown = true

      this.scene.add.existing(new Projectile({ scene: this.scene, damage: this.damage, velocity: 800, textureName: 'fireball', direction: this.direction, map: this.map, spawnPosition: new Phaser.Math.Vector2(this.x, this.y)}))

      setTimeout(() => {
        this.shootCooldown = false
      }, this.shootCooldownTime)
    }
  }
}