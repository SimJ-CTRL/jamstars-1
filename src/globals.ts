export const GAME_NAME = "project jamstars"

export const SCREEN_SIZE = Object.freeze({
  width: 960,
  height: 540
})

export const AXIS = Object.freeze({
  x: 'x',
  y: 'y'
})

export const IMAGE_BASE_PATH = "assets/images/"

export const GRAVITY: number = 1200